from threading import Thread
import math
import time
import threading
from multiprocessing import Process
import multiprocessing

# Exercice 1 (Multithreading)

def calcul_square(numbers):
    squared = []
    for number in numbers:
        time.sleep(0.2)
        squared.append(pow(number,2))
        print (squared)


def calcul_cube(numbers):
    cubic = []
    for number in numbers:
        time.sleep(0.2)
        cubic.append(pow(number,3))
        print (cubic)

if __name__ == "__main__":
    
    numbers = [2, 3, 8, 9, 12]
    t = time.time()

    t1 = threading.Thread(target = calcul_square, args=(numbers,))
    t2 = threading.Thread(target = calcul_cube, args=(numbers,))

    t1.start()
    t2.start()

    t1.join()
    t2.join()

# Exercice 2 (Multiprocessing)
print('multiprocessing')
def calcul_square2(numbers):
    squared = []
    for number in numbers:
        time.sleep(5)
        squared.append(pow(number,2))
        print (squared)


def calcul_cube2(numbers):
    cubic = []
    for number in numbers:
        time.sleep(5)
        cubic.append(pow(number,3))
        print (cubic)

if __name__ == "__main__":
    numbers = [2, 3, 4, 5, 6]
    proc1 = multiprocessing.Process(target= calcul_square2, args= (numbers,))
    proc2 = multiprocessing.Process(target= calcul_cube2, args= (numbers,))

    proc1.start()
    proc2.start()

    proc1.join()
    proc2.join()